#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <unistd.h>
#include "include.h"
#ifndef STDENV
#define STDENV "/bin/bash"
#endif
int main()
{
    #ifdef STDUID
    if(getuid() == STDUID) {
    #endif
    setuid( 0 );   // you can set it at run time also
    //printf("Current UID: %d\n",geteuid());
    system( STDENV );
    #ifdef STDUID
    }
    else {
    printf("Allow root user only %d uid. Current UID: %d.\n", STDUID, getuid());
    }
    #endif
    return 0;
}
