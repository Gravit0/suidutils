#include <iostream>
#include <string>
#include <fstream>
int main()
{
    std::cout << "Только один UID может получить(y/n):";
    char t1;
    std::cin >> t1;
    std::fstream file("include.h");
    if(t1 == 'y')
    {
        std::cout << "Какой UID может получить(int):";
        int uid = 0;
        std::cin >> uid;
        file << "#define STDUID " << std::to_string(uid) << std::endl;
    }
    std::cout << "Команда для выполнения:";
    std::string str;
    std::cin >> str;
    file << "#define STDENV \"" << str << "\"" << std::endl;
    system("gcc setuid.c -I./ -Wall -o bin");
    std::cout << "Установить SUID(y/n):";
    char t2;
    std::cin >> t2;
    if(t2 == 'y')
    {
        system("chmod +s bin");
    }
    return 0;
}
